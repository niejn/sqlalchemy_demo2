#-*- coding:utf-8 -*-

import datetime
import pandas as pd
import re
import time
import os
# from translate_v1 import
from helper.translate_v1 import translate
# from translate_v1 import translate

# futureID = re.compile(u'[a-zA-Z]+');
# multiplier = re.compile(u'\d+');
# for item in textlist:
#     id = futureID.findall(item)
#     mul = multiplier.findall(item)

def help_gen_type():
    data = pd.read_excel('./中证资本-仓单.xlsx', 'Sheet1', header=None)

    ori_db_keys = get_dbkeys('./d.txt')
    db_keys = {}
    db_types = {}
    data = data.iloc[:80, 0:3]

    ans = data.to_records(index=False)

    for a_record in ans:
        key = a_record[0]
        t_type = a_record[2]
        if key in ori_db_keys:
            db_keys[key] = ori_db_keys[key]
            # key_to_type[ori_db_keys[key]] = t_type
    return
def read_all(path):
    files = os.listdir(path)
    xls_list = []
    for file in files:
        file = path + '/' + file
        if not os.path.isfile(file):
            continue
        if file.endswith('xlsx'):
            xls_list.append(file)
            # srcFile.close()

    return xls_list

# 根据需求报告自动生成数据库建表sql
def gen_type(path=None):
    table_map = {'中证资本-仓单':'G_MANAGEMENT_WARRANT', '中证资本-场内股票':'G_MARKET_SHARES',
                 '中证资本-场外衍生':'G_OTC_DERIVATIVES', '中证资本-基差-点价':'G_BASIS_PRICING',
                 '中证资本-基差-期权':'G_BASIS_OPTION', '中证资本-期权做市':'TABLE G_OPTION_DEALER'}
    # path = './中证资本-仓单.xlsx'
    xls_name = os.path.splitext(os.path.split(path)[-1])[0]
    sheetname = 0
    # data = pd.read_excel(path, 'Sheet1',header=None)
    data = pd.read_excel(path, sheetname = 0,header=None)

    txt_name = xls_name + '.txt'
    db_table_name = table_map[xls_name]
    # data = data[:80]
    # iloc[1:5, 2:4]

    with open('./db_types.txt', 'r', encoding='utf-8', errors='ignore') as file:
        text = file.readlines()
    ori_db_types = {line.split()[0]:line.split(maxsplit=1)[1].strip(',\n') for line in text}
    with open("./type_map.txt", 'r', encoding='utf-8', errors='ignore') as file:
        text = file.readlines()
    type_map = {line.split('=')[0]:line.split('=')[1].strip() for line in text}
    ori_db_keys = get_dbkeys("./d.txt")
    db_keys = {}
    db_types = {}
    data = data.iloc[:80, 0:3]
    # print(data[2])
    # print(data)
    ans = data.to_records(index=False)


    key_to_type = {}
    for a_record in ans:
        # print(a_record)
        key = a_record[0]
        t_type = a_record[2]

        if key in ori_db_keys:
            db_keys[key] = ori_db_keys[key]
            key_to_type[ori_db_keys[key]] = t_type
        else:
            temp = translate(key)
            temp = temp.replace(" ", "_")
            db_keys[key] = temp.upper()
            key_to_type[temp.upper()] = t_type
            time.sleep(0.1)
    counter = {}
    for key in db_keys:
        counter[db_keys[key]] = 0
    for key in db_keys:
        if db_keys[key] in key_to_type:
            counter[db_keys[key]] += 1

    key_vals = list(db_keys.values())
    for key in key_to_type:
        if key in ori_db_types:
            key_to_type[key] = ori_db_types[key]
        else:
            if key_to_type[key] in type_map:
                key_to_type[key] = type_map[key_to_type[key]]
            else:
                print(key_to_type[key])
    # print(db_keys)sql_ddl.txt
    try:
        with open(txt_name, 'w') as file:
            str = 'CREATE TABLE {}\n'.format(db_table_name)
            file.write(str)
            str = '(\n'
            file.write(str)
            for key in key_to_type:
                str = '\t' + key + ' ' + key_to_type[key] + ',\n'
                file.write(str)
            str = ');\n'.format(db_table_name)
            file.write(str)
            for key in db_keys:
                str = "COMMENT ON COLUMN {}.{} IS '{}';\n".format(db_table_name, db_keys[key], key)
                file.write(str)
    except Exception as e:
        print(e)
    return


# 从sql comment 中提取数据库中表字段与汉字解释之间的对应关系。
def get_dbkeys(path='./T_OWN_INVEST_DERIV.txt'):
    str = '权利金（若为买入期权）'
    ans = str.split("（")
    str = '持仓日期(数据日期)'
    ans = str.split("(")
    with open(path, 'r',encoding='utf-8', errors='ignore') as file:
        # res = file.read()
        res = file.readlines()
        # print(res)
    # COMMENT ON COLUMN T_OWN_INVEST_DERIV.REPORT_DATE IS '持仓日期(数据日期)';
    # res_tr = r"COMMENT ON COLUMN ACCOUNT_SUMMARY.(.*?) IS '(.*?)'"
    # res_tr = r"COMMENT ON COLUMN (.*[.])(.*)IS '(.*)'"
    # COMMENT ON COLUMN ACCOUNT_SUMMARY.DEPOSIT_WITHDRAWAL  IS  '出 入 金';
    # res_tr = r"COMMENT ON COLUMN POSITION.(.*?) IS '(.*?)'"

    # COMMENT ON COLUMN POSITION.TRADING_DAY  IS   '交易日';
    # COMMENT ON COLUMN POSITION.(.*?) IS   '(.*?)';
    res_tr = r"COMMENT ON COLUMN POSITION.(.*?) IS   '(.*?)';"
    urls = re.findall(res_tr, res[0], re.I | re.S | re.M)
    m_tr = re.findall(res_tr, res[0], re.S | re.M)
    keypairs = [re.findall(res_tr, key, re.S | re.M) for key in res if re.findall(res_tr, key, re.S | re.M)]
    print(keypairs)
     # keydict = dict((apair[0], apair[1]) for apair in keypairs)
    # for apair in keypairs:
    #     print(apair[0][0])
    #     print(apair[0][1])
    keydict = {apair[0][1].split("(")[0]:apair[0][0]  for apair in keypairs}
    # keydict = {apair[0][2]: apair[0][1].strip().lower() for apair in keypairs}
    print(keydict)
    # path = './warrant_table.txt'
    # with open(path, 'r', encoding='utf-8', errors='ignore') as file:
    #     # res = file.read()
    #     res = file.readlines()
    #     print(res)

    long_key_pairs = [line.strip().split()[0] for line in res if line.strip().split()]
    # long_key_dict = {apair[1]: apair[0] for apair in long_key_pairs}
    index = 0
    # tkeys  = list(keydict.keys())

    # for key in long_key_pairs:
    #     if key in keydict:
    #         tkeys.remove(key)
    #         print(index)
    #         index += 1
    #         print(key)
    #         print(long_key_dict[key])
    #         print(keydict[key])
    #         long_key_dict[key] = keydict[key]
    # print(tkeys)
    # ans = {}
    # gap =  ''
    # keydict_keys = keydict.keys()
    # for key in long_key_pairs:
    #     if key in keydict_keys:
    #         ans[key] = keydict[key]
    #     else:
    #         temp = translate(key)
    #         temp = temp.replace(" ", "_")
    #         ans[key] = temp.upper()
    #         time.sleep(0.5)
    #     # print(index, gap, key, gap, ans[key])
    #     # index += 1
    #     print(ans[key])
    return keydict


def cr_comments(keys, notes):
    key_list = [key.strip().split() for key in keys.split('\n')]
    key_list_1 = [key[0] for key in key_list]
    note_list = [note.strip() for note in notes.split('\n')]
    for key, note in zip(key_list_1, note_list):
        ans = "COMMENT ON COLUMN G_OWN_INVEST_NEEQ.%s IS '%s';" % (key, note)

        print(ans)
        # match_list.append(ans)
    return


def match(keys, notes):
    # key_list = [key.strip().split() for key in keys.split('\n')]
    # key_list_1 = [key[0] for key in key_list]
    # # note_list = [
    # print(key_list)
    # print(key_list_1)
    keys_list = [key.strip() for key in keys.split('\n')]
    note_list = [note.strip() for note in notes.split('\n')]
    match_list = []
    # COMMENT ON TABLE G_OWN_INVEST_NEEQ IS '中证资本新三板业务数据接口表';
    # COMMENT ON COLUMN G_OWN_INVEST_NEEQ.REPORT_DATE IS '持仓日期(数据日期)';
    for key, note in zip(keys_list, note_list):
        ans = "%s--%s" % (key, note)

        # print(ans)
        match_list.append(ans)

    for key, note in zip(keys_list, note_list):
        ans = "%s--%s"%(key, note)

        # print(ans)
        match_list.append(ans)

    print(match_list)





    return

# 根据数据库字段设置pandas对应字段的类型
def set_type(path):
    with open(path, 'r',encoding='utf-8', errors='ignore') as file:
        # res = file.read()
        res = file.readlines()
    # headers = [line.split()[0].lower() for line in res]
    # df['col2'] = df['col2'].astype('float64')
    # image_name_data['id'] = image_name_data['id'].astype(int).astype('str')
    type_map = {'Date': 'object', 'VARCHAR': 'str', 'NUMBER': 'float64', 'INTEGER': 'int', 'CHAR': 'str'}
    ans_type_map = {}
    for line in res:
        tlines = line.split()
        print(tlines)
        t_type = tlines[1].strip()
        t_key = tlines[0].strip()
        for a_type in type_map:
            if a_type.upper() in t_type.upper():
                ans_type_map[t_key.upper()] = type_map[a_type]
    print(ans_type_map)
    # {'LIQUIDATION_AMOUNT': 'float64', 'MANAGE_MODE': 'str', 'MANAGE_ASSET_VALUE': 'float64', 'PRODUCT_CODE': 'str',
    #  'BUSINESS_TYPE': 'str', 'PRODUCT_TYPE': 'str', 'CF_PRODUCT_TYPE': 'str', 'MANAGE_COMPANY_CODE': 'str',
    #  'REPORT_DATE': 'int', 'PRODUCT_NAME': 'str', 'CURRENT_NAV': 'float64', 'CF_PRODUCT_SUB_TYPE1': 'str',
    #  'WINDING_UP_WARNING': 'float64', 'CF_PRODUCT_SUB_TYPE3': 'str', 'CREDIT_WARNING': 'float64',
    #  'CURRENT_NAV_O32': 'float64', 'LAST_MODIFIED_DATE': 'object', 'CF_PRODUCT_SUB_TYPE2': 'str',
    #  'MANAGE_ASSET_VALUE_O32': 'float64'}

    return

# --add default on 0918
# alter table G_MANAGE_ASSET_INFO modify CF_PRODUCT_TYPE default 'NONE';
# ALTER TABLE G_OPTION_DEALER modify LAST_MODIFIED_DATE default sysdate;
# a = "I'm %s. I'm %d year old" % ('Vamei', 99)
# 根据数据库字段设置字段的默认值
def set_default(path, table_name):
    with open(path, 'r',encoding='utf-8', errors='ignore') as file:
        # res = file.read()
        res = file.readlines()
    # headers = [line.split()[0].lower() for line in res]
    # df['col2'] = df['col2'].astype('float64')
    # image_name_data['id'] = image_name_data['id'].astype(int).astype('str')
    type_map = {'Date': 'sysdate', 'VARCHAR': "'0'", 'NUMBER': '0', 'INTEGER': '0' }
    ans_type_map = {}
    for line in res:
        tlines = line.split()
        # print(tlines)
        t_type = tlines[1].strip()
        t_key = tlines[0].strip()
        for a_type in type_map:
            if a_type.upper() in t_type.upper():
                ans_str = "ALTER TABLE %s modify %s default %s;" % (table_name, t_key.upper(), type_map[a_type])
                print(ans_str)
                # ans_type_map[t_key.lower()] = type_map[a_type]


    return


def get_sql_headers(path):
    with open(path, 'r',encoding='utf-8', errors='ignore') as file:
        # res = file.read()
        res = file.readlines()
    # headers = [line.split()[0].lower() for line in res]
    headers_list = [line.split()[0].lower() for line in res]
    print(headers_list)
    ans_str = ''
    for a_header in headers_list:
        ans_str += "'" + a_header + "',"

    print(ans_str)

    for a_header in headers_list:
        ans_str += 'dealer.c.' + a_header + ', '

    print(ans_str)

    headers = {line.split()[0].lower():line.split()[1].strip().lower() for line in res}
    return headers
#  compare the trigger headers with the dbtable headers
def comp_headers(db_path, citic_path):
    dbheaders = get_sql_headers(db_path)
    trig = get_sql_headers(citic_path)
    for trig_header in trig:
        if trig_header not in dbheaders:
            print(trig_header)
        else:
            if dbheaders[trig_header] != trig[trig_header]:
                print('*' * 100)
                print(trig_header)
                print(dbheaders[trig_header])
                print(trig[trig_header])
                print('*' * 100)
    return

# test pandas type setting

def pandas_read(path):
    t_types = {'manage_mode': 'str', 'product_type': 'str', 'cf_product_sub_type2': 'str',
               'cf_product_sub_type1': 'str', 'cf_product_sub_type3': 'str',
               'manage_asset_value_o32': 'float64', 'product_code': 'str',
               'cf_product_type': 'str', 'liquidation_amount': 'float64',
               'manage_company_code': 'str', 'current_nav': 'float64',
               'manage_asset_value': 'float64', 'winding_up_warning': 'float64',
               'product_name': 'str', 'last_modified_date': 'object', 'credit_warning': 'float64',
               'business_type': 'str', 'current_nav_o32': 'float64'}
    if path.endswith('xls') or path.endswith('xlsx'):
        df = pd.read_excel(path, header=0, skip_footer=1)
    else:
        # df = pd.read_csv(config_path, header=0, skipfooter=1, encoding='utf-8', engine='python')
        df = pd.read_csv(path, header=0, encoding='utf-8')
        df = df.iloc[:-1, :]

    return

# 根据字段名称翻译并自动生成建表sql
def gen_sql_fr_headers():
    headers = '''交易日	交易所	投资者代码	投资者名称	资金账号	合约代码	合约编码	投机套保标志	持仓类型	备兑标志	证券账户	期权子账户	权利仓持仓量	义务仓持仓量	非组合多头持仓量	非组合空头持仓量	投资者保证金	投资者多头保证金	投资者空头保证金	交易所保证金	组合合约多头持仓量	组合合约空头持仓量	交易所多头保证金	交易所空头保证金	
结算价	买入成本	卖出成本	昨多头持仓量	昨空头持仓量	合约数量乘数	期权多头持仓市值	期权空头持仓市值	昨结算价	标的证券编码'''
    headers = headers.split()
    table_map = {'中证资本-仓单':'G_MANAGEMENT_WARRANT', '中证资本-场内股票':'G_MARKET_SHARES',
                 '中证资本-场外衍生':'G_OTC_DERIVATIVES', '中证资本-基差-点价':'G_BASIS_PRICING',
                 '中证资本-基差-期权':'G_BASIS_OPTION', '中证资本-期权做市':'TABLE G_OPTION_DEALER'}
    # path = './中证资本-仓单.xlsx'



    with open('./db_types.txt', 'r', encoding='utf-8', errors='ignore') as file:
        text = file.readlines()
    ori_db_types = {line.split()[0]:line.split(maxsplit=1)[1].strip(',\n') for line in text}
    with open("./type_map.txt", 'r', encoding='utf-8', errors='ignore') as file:
        text = file.readlines()
    type_map = {line.split('=')[0]:line.split('=')[1].strip() for line in text}
    ori_db_keys = get_dbkeys("./position_sql.txt")
    db_keys = {}
    db_types = {}

    # 字符串11
    str_headers = ['交易所', '投资者名称', '合约代码','合约编码', '投资者代码', '资金账号', '投机套保标志', '持仓类型', '备兑标志', '证券账户', '期权子账户']
    # 浮点数21
    float_headers = ['权利仓持仓量','义务仓持仓量', '非组合多头持仓量', '非组合空头持仓量', '投资者保证金', '投资者多头保证金', '投资者空头保证金'
                     ,'交易所保证金', '组合合约多头持仓量', '组合合约空头持仓量', '交易所多头保证金', '交易所空头保证金', '结算价',
                     '买入成本', '卖出成本', '昨多头持仓量', '昨空头持仓量', '期权多头持仓市值', '期权空头持仓市值', '昨结算价', '标的证券编码']
    date_headers = ['']
    # 整数2
    int_headers = ['交易日', '合约数量乘数']
    key_to_type = {}
    for key in headers:

        if key in str_headers:
            t_type = 'VARCHAR2(64 BYTE)'
        elif key in float_headers:
            t_type = 'NUMBER(20,2)'
        elif key in date_headers:
            t_type = 'DATE'
        else:
            t_type = 'INTEGER'


        if key in ori_db_keys:
            db_keys[key] = ori_db_keys[key]
            key_to_type[ori_db_keys[key]] = t_type
        else:
            temp = translate(key)
            temp = temp.replace(" ", "_")
            db_keys[key] = temp.upper()
            key_to_type[temp.upper()] = t_type
            time.sleep(0.1)

    # key_to_type = {}
    # for a_record in headers:
    #     # print(a_record)
    #     key = a_record[0]
    #     t_type = a_record[2]
    #
    #     if key in ori_db_keys:
    #         db_keys[key] = ori_db_keys[key]
    #         key_to_type[ori_db_keys[key]] = t_type
    #     else:
    #         temp = translate(key)
    #         temp = temp.replace(" ", "_")
    #         db_keys[key] = temp.upper()
    #         key_to_type[temp.upper()] = t_type
    #         time.sleep(0.1)
    counter = {}
    for key in db_keys:
        counter[db_keys[key]] = 0
    for key in db_keys:
        if db_keys[key] in key_to_type:
            counter[db_keys[key]] += 1

    key_vals = list(db_keys.values())
    # for key in key_to_type:
    #     if key in ori_db_types:
    #         key_to_type[key] = ori_db_types[key]
    #     else:
    #         if key_to_type[key] in type_map:
    #             key_to_type[key] = type_map[key_to_type[key]]
    #         else:
    #             print(key_to_type[key])
    # print(db_keys)sql_ddl.txt
    txt_name = './etf.txt'
    db_table_name = 'etfposition'
    try:
        with open(txt_name, 'w') as file:
            str = 'CREATE TABLE {}\n'.format(db_table_name)
            file.write(str)
            str = '(\n'
            file.write(str)
            for key in key_to_type:
                str = '\t' + key + ' ' + key_to_type[key] + ',\n'
                file.write(str)
            str = ');\n'.format(db_table_name)
            file.write(str)
            for key in db_keys:
                str = "COMMENT ON COLUMN {}.{} IS '{}';\n".format(db_table_name, db_keys[key], key)
                file.write(str)
    except Exception as e:
        print(e)
    return

def main():
    gen_sql_fr_headers()
    # comp_headers('./qiquanzuoshi.txt', "./trigger.txt")
    # changwaiyanshengheader.txt
    # set_type("./qihuochicangheader.txt")
    # set_default(path, table_name):
    # set_default('./xsb_loc.txt', 'G_OWN_INVEST_NEEQ')
    # set_default('./xsb_rem.txt', 'T_OWN_INVEST_NEEQ')
    # cwys_loc.txt G_OTC_DERIVATIVES
    # set_default('./qqzs_loc', 'G_OPTION_DEALER')
    # pandas_read('./test_de.xlsx')
    # set_type("./xsb_loc")
    # set_type("./changwaiyanshengheader.txt")
    # get_sql_headers("./yanshenpin.txt")
    # get_dbkeys('./qihuochicangcom.txt')
    # file_list = read_all('../requirements')
    # for file in file_list:
    #     gen_type(file)
    return

if __name__ == "__main__":
    main()