CREATE TABLE G_MARKET_SHARES
(
	SECURITIES_ACCOUNT_TOTAL_PROFIT_AND_LOSS NUMBER(20,2),
	THE_BALANCE NUMBER(20,2),
	STOCK_MARKET_CAPITALIZATION_SECURITIES_ACCOUNT NUMBER(20,2),
	SECURITIES_ACCOUNT_BALANCES NUMBER(20,2),
	SECURITIES_HOLDINGS_WORTH NUMBER(20,2),
	NUMBER_OF_SECURITIES_HOLDINGS NUMBER(20,2),
	DISCREPANCY_GOLD_SECURITIES_ACCOUNT NUMBER(20,2),
	STOCK_CODE VARCHAR2(64 BYTE),
	SECURITIES_HOLDINGS_COST NUMBER(20,2),
	NAME_OF_THE_SECURITIES VARCHAR2(64 BYTE),
	SECURITIES_ACCOUNT_NUMBER VARCHAR2(64 BYTE),
	SECURITIES_AT_THE_BEGINNING_OF_TOTAL_ASSETS NUMBER(20,2),
	SECURITIES_HAS_TO_BREAK_EVEN NUMBER(20,2),
	SECURITIES_HOLDINGS_PROFIT_AND_LOSS NUMBER(20,2),
	REFER_TO_THE_MARKET_PRICE NUMBER(20,2),
	REFER_TO_THE_COST_PRICE NUMBER(20,2),
	NUMBER_OF_FROZEN NUMBER(20,2),
	SECURITIES_AT_THE_END_OF_THE_BEGINNING NUMBER(20,2),
);
COMMENT ON COLUMN G_MARKET_SHARES.SECURITIES_AT_THE_END_OF_THE_BEGINNING IS '证券期初末资产';
COMMENT ON COLUMN G_MARKET_SHARES.SECURITIES_HOLDINGS_PROFIT_AND_LOSS IS '证券持仓盈亏';
COMMENT ON COLUMN G_MARKET_SHARES.NUMBER_OF_FROZEN IS '冻结数量';
COMMENT ON COLUMN G_MARKET_SHARES.SECURITIES_ACCOUNT_NUMBER IS '证券账户编号';
COMMENT ON COLUMN G_MARKET_SHARES.THE_BALANCE IS '资金余额';
COMMENT ON COLUMN G_MARKET_SHARES.SECURITIES_HOLDINGS_WORTH IS '证券持仓市值';
COMMENT ON COLUMN G_MARKET_SHARES.REFER_TO_THE_MARKET_PRICE IS '参考市价';
COMMENT ON COLUMN G_MARKET_SHARES.STOCK_MARKET_CAPITALIZATION_SECURITIES_ACCOUNT IS '证券账户股票总市值';
COMMENT ON COLUMN G_MARKET_SHARES.SECURITIES_ACCOUNT_TOTAL_PROFIT_AND_LOSS IS '证券账户总盈亏';
COMMENT ON COLUMN G_MARKET_SHARES.SECURITIES_HAS_TO_BREAK_EVEN IS '证券已实现盈亏';
COMMENT ON COLUMN G_MARKET_SHARES.REFER_TO_THE_COST_PRICE IS '参考成本价';
COMMENT ON COLUMN G_MARKET_SHARES.NAME_OF_THE_SECURITIES IS '证券名称';
COMMENT ON COLUMN G_MARKET_SHARES.SECURITIES_AT_THE_BEGINNING_OF_TOTAL_ASSETS IS '证券期初总资产';
COMMENT ON COLUMN G_MARKET_SHARES.NUMBER_OF_SECURITIES_HOLDINGS IS '证券持仓数量';
COMMENT ON COLUMN G_MARKET_SHARES.SECURITIES_ACCOUNT_BALANCES IS '证券账户资金余额';
COMMENT ON COLUMN G_MARKET_SHARES.SECURITIES_HOLDINGS_COST IS '证券持仓成本';
COMMENT ON COLUMN G_MARKET_SHARES.STOCK_CODE IS '证券代码';
COMMENT ON COLUMN G_MARKET_SHARES.DISCREPANCY_GOLD_SECURITIES_ACCOUNT IS '证券账户出入金';
