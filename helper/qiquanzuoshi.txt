  CURRENCY_CODE VARCHAR2(16 BYTE),
  CP_INTER_RATING VARCHAR2(16 BYTE),
  EFFECTIVE_RESPONSE_INQUIRY NUMBER(20,2),
  BUSINESS_APPLICATION NUMBER(20,2),
  GROUP_COUNTER_PARTY VARCHAR2(256 BYTE),
  GAMMA NUMBER(20,2),
  DELTA_VALUE number(20,4),
  FUT_DEVBY_OPT_CP_RD_IV NUMBER(20,2),
  LATEST_PRICE NUMBER(20,2),
  MAIN_CONTRACT_HISTORY_21_DAYS NUMBER(20,2),
  RHO NUMBER(20,2),
  GROUP_INTER_TRANS_FLAG VARCHAR2(1 BYTE),
  LAST_MODIFIED_DATE DATE,
  RISK_EVENTS VARCHAR2(64 BYTE),
  EFFCT_CONTINUOUS_PRICE_RATIO NUMBER(20,2),
  PRESS_LOSS NUMBER(20,4),
  CP_CRED_TYPE VARCHAR2(64 BYTE),
  BUSINESS_USE_MONEY NUMBER(20,2),
  DERIVATIVES_ACCOUNT_NUMBER VARCHAR2(64 BYTE),
  CAPITAL_OCCUPANCY_RATE NUMBER(20,2),
  DERIVATIVES_TYPE VARCHAR2(16 BYTE),
  DELTA NUMBER(20,2),
  COUNTER_PARTY VARCHAR2(128 BYTE),
  EXCHANGE_TYPE VARCHAR2(16 BYTE),
  ON_EXCHANGE_FLAG VARCHAR2(2 BYTE),
  REPORT_DATE INTEGER,
  AMOUN_IN_OUT NUMBER(20,2),
  IMPLIED_VOLATILITY NUMBER(20,2),
  EXCHANGE_RATE NUMBER(20,2),
  MAIN_CONTRACT_HISTORY_282_DAY NUMBER(20,2),
  BUSINESS_DISCREPANCY VARCHAR2(64 BYTE),
  NOTIONAL_VALUE NUMBER(20,2),
  MAIN_CONTRACT_LPRICE NUMBER(20,2),
  PREMIUM_PRICE NUMBER(20,4),
  BUSINESS_TYPES VARCHAR2(64 BYTE),
  CP_CRED_ID VARCHAR2(64 BYTE),
  VEGA NUMBER(20,2),
  THE_MAIN_CONTRACT NUMBER(20,2),
  THETA NUMBER(20,2),
  BOOK_VALUE NUMBER(20,2),
  GROUP_COUNTER_VALUE NUMBER(20,2),
  MANAGE_COMPANY_CODE VARCHAR2(256 BYTE)