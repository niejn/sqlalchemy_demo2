#coding: utf-8
import urllib.request
import urllib.parse
import json

import time


def get_data(words):
    data = {}
    data["type"] = "AUTO"
    data["i"] = words
    data["doctype"] = "json"
    data["xmlVersion"] = "1.8"
    data["keyfrom:fanyi"] = "web"
    data["ue"] = "UTF-8"
    data["action"] = "FY_BY_CLICKBUTTON"
    data["typoResult"] = "true"
    data = urllib.parse.urlencode(data).encode('utf-8')
    return data


def url_open(url, data):
    req = urllib.request.Request(url, data)
    req.add_header("User-Agent", "Mozilla/4.0 (Windows NT 6.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36")
    response = urllib.request.urlopen(req)
    html = response.read()
    html = html.decode("utf-8")
    return html


def get_json_data(html):
    result = json.loads(html)
    result = result['translateResult']
    result = result[0][0]['tgt']
    return result


def translate(words):
    # https://www.baidu.com/s?ie=utf-8&f=3&rsv_bp=0&rsv_idx=1&tn=baidu&wd=steam&rsv_pq=d876117c00003234&rsv_t=58afJfIfLuISO9CUIMqOhJsmNYx9ep3VIRwTVMEvQoToHFi3fFQF4R9YvWA&rqlang=cn&rsv_enter=1&rsv_sug3=2&rsv_sug1=1&rsv_sug7=100&rsv_sug2=0&prefixsug=s&rsp=0&inputT=2222&rsv_sug4=2716
    time.sleep(0.1)
    url = "http://fanyi.youdao.com/translate?smartresult=dict&smartresult=rule&smartresult=ugc&sessionFrom=dict.top"
    data = get_data(words)
    html = url_open(url, data)
    result = get_json_data(html)
    return result

# rU 或 Ua 以读方式打开, 同时提供通用换行符支持 (PEP 278)
# w     以写方式打开，
# a     以追加模式打开 (从 EOF 开始, 必要时创建新文件)
# r+     以读写模式打开
# w+     以读写模式打开 (参见 w )
# a+     以读写模式打开 (参见 a )
# rb     以二进制读模式打开
# wb     以二进制写模式打开 (参见 w )
# ab     以二进制追加模式打开 (参见 a )
# rb+    以二进制读写模式打开 (参见 r+ )
# wb+    以二进制读写模式打开 (参见 w+ )
# ab+    以二进制读写模式打开 (参见 a+ )
def get_group_tanslate(path='./b.txt'):
    with open(path, 'r',encoding='utf-8', errors='ignore') as file:
        # res = file.read()
        res = file.readlines()
        # print(res)
    #     '集团内部交易对手（如有）' '汇率(兑人民币)'
    import re
    # a = 'Beautiful, is; better*than\nugly'
    # # 四个分隔符为：,  ;  *  \n
    # x = re.split(',|; |\*|\n', a)
    # print(x)
    words = [line.strip() for line in res]
    words = [re.split("（|\(", word)[0] for word in words]
    # words1 = re.split("\(", '持仓日期(数据日期)')[0]
    print(words)
    for word in words:
        if word:
            ans = translate(word)
            ans = ans.replace(" ","_")
            ans = ans.upper()
            print(ans, " ", word)
            time.sleep(0.5)
        # en_words = [translate(word) for word in words]
    return


def main():

    get_group_tanslate()
    # words = input("please input words: ")
    # url = "http://fanyi.youdao.com/translate?smartresult=dict&smartresult=rule&smartresult=ugc&sessionFrom=dict.top"

    # data = get_data(words)
    # html = url_open(url, data)
    # result = get_json_data(html)

    # result = translate(words)
    # print("The result: %s" % result)

if __name__ == "__main__":
    while True:
        main()
